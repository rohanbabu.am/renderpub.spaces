/*
 * Author: Rohan Babu A M
 * Renderpub, March 2022
 */

// express will run our server
const express = require("express");

https = require('https');
fs = require('fs');
const app = express();
app.use(express.static("public"));

// decide on which port we will run
const port = process.env.PORT || 3000;
const host = '127.0.0.1';

var options = {
    key  : fs.readFileSync('ssl/key.pem'),
    ca   : fs.readFileSync('ssl/csr.pem'),
    cert : fs.readFileSync('ssl/cert.pem')
}


const server = https.createServer(options, app)

server.listen(port, function() {
    //console.log('Server listening on port %d in %s mode', port, app.settings.env);
	console.log("Server is running on https://localhost:" + port)
});

/////SOCKET.IO///////
const io = require("socket.io")().listen(server);

// an object where we will store innformation about active clients
let peers = {};


function main() {
  setupSocketServer();

  setInterval(function () {
    // update all clients of positions
    io.sockets.emit("positions", peers);
  }, 10);
}

main();

function setupSocketServer() {
  // Set up each socket connection
  io.on("connection", (socket) => {
    console.log(
      "Peer joined with ID",
      socket.id,
      ". There are " +
      io.engine.clientsCount +
      " peer(s) connected."
    );

    //Add a new client indexed by their socket id
    peers[socket.id] = {
      position: [0, 0.5, 0],
      rotation: [0, 0, 0, 1], // stored as XYZW values of Quaternion
    };

    // Make sure to send the client their ID and a list of ICE servers for WebRTC network traversal
    socket.emit(
      "introduction",
      Object.keys(peers)
    );

    // also give the client all existing clients positions:
    socket.emit("userPositions", peers);

    //Update everyone that the number of users has changed
    io.emit(
      "newUserConnected",
      socket.id
    );

    // whenever the client moves, update their movements in the clients object
    socket.on("move", (data) => {
      if (peers[socket.id]) {
        peers[socket.id].position = data[0];
        peers[socket.id].rotation = data[1];
      }
    });

    // Relay simple-peer signals back and forth
    socket.on("signal", (to, from, data) => {
      if (to in peers) {
        io.to(to).emit("signal", to, from, data);
      } else {
        console.log("Peer not found!");
      }
    });

    //Handle the disconnection
    socket.on("disconnect", () => {
      //Delete this client from the object
      delete peers[socket.id];
      io.sockets.emit(
        "userDisconnected",
        io.engine.clientsCount,
        socket.id,
        Object.keys(peers)
      );
      console.log(
        "User " +
        socket.id +
        " diconnected, there are " +
        io.engine.clientsCount +
        " clients connected"
      );
    });
  });
}
