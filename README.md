# renderpub.spaces

Renderpub Spaces Tech Demo

## Quickstart:

1. Install dependencies:   
    npm install
	
2. Start the server:  
    npm start
	
3. On your browser, navigate to  
    `http://localhost:3000`.
	
	
## Quickstart:
3D model files are removed from this repository for licensing reasons  
Please edit main.js file to load your own 3D model file.
