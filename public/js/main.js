/*
 * Author: Rohan Babu A M
 * Renderpub, March 2022
 */
import renderPubNFT from '../utils/renderPubNFT.json' assert { type: "json" };
// Web3 object
let currentAccount;


let THREE
let FirstPersonControls
let clock

let mySocket;

// array of connected clients
let peers = {};
let headMesh, bodyMesh;
let headSection, bodySection;

// Variable to store our three.js scene:
let myScene;
let GLTFLoader;
let DRACOLoader;
let loader, dracoLoader;
let RGBELoader;

let FBXLoader, FBXLoader1;

let CSS2DRenderer, CSS2DObject, labelRenderer;

// set video width / height / framerate here:
const videoWidth = 800;
const videoHeight = 600;
const videoFrameRate = 24;


// Our local media stream (i.e. webcam and microphone stream)
let localMediaStream = null;

// Constraints for our local audio/video stream
let mediaConstraints = {
  audio: true,
  video: {
    width: videoWidth,
    height: videoHeight,
    frameRate: videoFrameRate,
  },
};


class Scene {
  constructor(modelPath) {
	this.modelPath = modelPath
	
    //THREE scene
    this.scene = new THREE.Scene();
    //Utility
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    // lerp value to be used when interpolating positions and rotations
    this.lerpValue = 0;

    //THREE Camera
    this.camera = new THREE.PerspectiveCamera(
      45,
      this.width / this.height,
      0.1,
      5000
    );
    this.camera.position.set(0, 3, 6);
    this.scene.add(this.camera);

    // create an AudioListener and add it to the camera
    this.listener = new THREE.AudioListener();
    this.camera.add(this.listener);

    //THREE WebGL renderer
    this.renderer = new THREE.WebGLRenderer({
      antialiasing: true,
    });
    this.renderer.setClearColor(new THREE.Color("lightblue"));
    this.renderer.setSize(this.width, this.height);
	this.renderer.setPixelRatio( window.devicePixelRatio );
	
	this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
	this.renderer.toneMappingExposure = 1.1;
	this.renderer.outputEncoding = THREE.sRGBEncoding;
	
	// add controls:
    this.controls = new FirstPersonControls(THREE, this.scene, this.camera, this.renderer);
    //Push the canvas to the DOM
    let domElement = document.getElementById("canvas-container");
	domElement.append(this.renderer.domElement);
	

    //Setup event listeners for events and handle the states
    window.addEventListener("resize", (e) => this.onWindowResize(e), false);

    // Helpers
    //this.scene.add(new THREE.GridHelper(500, 500));
    //this.scene.add(new THREE.AxesHelper(10));

    this.addLights();
	this.createEnvironment();

    // Start the loop
    this.frameCount = 0;
    this.update();
  }

	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	// Load Environment from path
	createEnvironment() {
		console.log("Adding environment");
		new RGBELoader().load( '../assets/brutalism_scene/sunset_in_the_chalk_quarry_2k.hdr', function ( texture ) {

				texture.mapping = THREE.EquirectangularReflectionMapping;

				this.scene.background = texture;
				this.scene.environment = texture;
			}.bind(this) );

		// Load a glTF resource
		loader.load( this.modelPath, function ( gltf ) {
			
			gltf.scene.scale.set(0.0016,0.0016,0.0016)
			gltf.scene.position.set(0.0,-15.0,0.0) 
			this.scene.add( gltf.scene );
			

			//update();
			//render();

		}.bind(this),
		function ( xhr ) {

			console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

		},
		// called when loading has errors
		function ( error ) {

			console.log( 'An error happened' );

		} );
		
	}
	
	
  //////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////
  // Lighting 💡

  addLights() {
    this.scene.add(new THREE.AmbientLight(0xffffe6, 0.7));
  }

  //////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////
  // Clients 👫

  // add a client meshes, a video element and  canvas for three.js video texture
  addClient(id) {
	
	headSection = new THREE.Group();
	bodySection = new THREE.Group();
	
	let headgroup = new THREE.Group();
	let bodygroup = new THREE.Group();
	
	
	let videoMaterial = this.makeVideoMaterial(id);
    let otherMat = new THREE.MeshNormalMaterial();
	var HeadMaterial = new THREE.MeshStandardMaterial( {color: 0x291b1a, roughness: 0.2, metalness:0.5} );
	var BodyMaterial = new THREE.MeshStandardMaterial( {color: 0x544342, roughness: 0.2, metalness:0.8} );
	
	var ct = 0;
	
	if(headMesh.scene){
		console.log("valid")
	}
	
	
	headMesh.scene.traverse(function (child){
		if ( child instanceof THREE.Mesh ) {
			if(ct==0){
				child.material = videoMaterial
			}
			else{
				child.material = BodyMaterial
			}
			//headSection.add(child);
			ct++;
        }
	});
	headSection = headMesh.scene;
	
	
	bodyMesh.scene.traverse(function (child){
		if ( child instanceof THREE.Mesh ) {
			child.material = BodyMaterial
			//bodySection.add(child);
        }
	});
	bodySection = bodyMesh.scene;
	
	//headSection.position.set(0.05,-0.0,0.07)
	//bodySection.position.set(0.0,-0.25,0.0)
	
	//headSection.name = "head"
	//bodySection.name = "body"
	


    let head = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), [HeadMaterial,HeadMaterial,HeadMaterial,HeadMaterial,HeadMaterial,videoMaterial]);
	let body = new THREE.Mesh(new THREE.BoxGeometry(0.3, 1.5, 0.3), [BodyMaterial,BodyMaterial,BodyMaterial,BodyMaterial,BodyMaterial,BodyMaterial]);
	
	head.position.set(0.0,0.0,0.0)
	body.position.set(0.0,-0.75,0.0)
	
	
	
	headgroup.add(head);
	bodygroup.add(body)
	
	headgroup.name = "head"
	bodygroup.name = "body"
	
    // add group to scene
    this.scene.add(headgroup);
	this.scene.add(bodygroup);

    peers[id].head = headgroup;
	peers[id].body = bodygroup;	
	
    peers[id].previousPosition = new THREE.Vector3();
    peers[id].previousRotation = new THREE.Quaternion();
	peers[id].desiredPosition = new THREE.Vector3();
    peers[id].desiredRotation = new THREE.Quaternion();
	
  }

  removeClient(id) {
    this.scene.remove(peers[id].head);
	this.scene.remove(peers[id].body);
  }

  // overloaded function can deal with new info or not
  updateClientPositions(clientProperties) {
    this.lerpValue = 0;
    for (let id in clientProperties) {
      if (id != mySocket.id) {
        peers[id].previousPosition.copy(peers[id].head.position);
        peers[id].previousRotation.copy(peers[id].head.quaternion);
        peers[id].desiredPosition = new THREE.Vector3().fromArray( clientProperties[id].position );
        peers[id].desiredRotation = new THREE.Quaternion().fromArray( clientProperties[id].rotation );
      }
	  else{
		  //console.log(peers[id].group)
	  }
    }
  }

/*
  interpolatePositions() {
    this.lerpValue += 0.1; // updates are sent roughly every 1/5 second == 10 frames
    for (let id in peers) {
      if (peers[id].group) {
		// sanity checking  
		if( peers[id].previousPosition && peers[id].desiredPosition ){
			peers[id].group.position.lerpVectors(peers[id].previousPosition,peers[id].desiredPosition, this.lerpValue);
			peers[id].body.position.lerpVectors(peers[id].previousPosition,peers[id].desiredPosition, this.lerpValue);
		}
        
		if(peers[id].previousRotation && peers[id].desiredRotation){
			if(peers[id].group.children.length == 2){
			if( peers[id].group.children[0].name == "head" ){
				//peers[id].group.children[0].quaternion.slerpQuaternions(peers[id].previousRotation,peers[id].desiredRotation, this.lerpValue);
			}
			else{
				//peers[id].group.children[1].quaternion.slerpQuaternions(peers[id].previousRotation,peers[id].desiredRotation, this.lerpValue);
			}
			
		}
		//console.log(peers[id].desiredRotation)
		peers[id].group.quaternion.slerpQuaternions(peers[id].previousRotation,peers[id].desiredRotation, this.lerpValue);
		// check and limit camera head rotation here
		if(peers[id].group.rotation.x < 0.0 && peers[id].group.rotation.x > -2.35){
			//peers[id].group.rotation.x = -2.35;
		}
			
		}
		
      }
    }
  }
*/


  interpolatePositions() {
    this.lerpValue += 0.1; // updates are sent roughly every 1/5 second == 10 frames
    for (let id in peers) {
      if (peers[id].head) {
        peers[id].head.position.lerpVectors(peers[id].previousPosition,peers[id].desiredPosition, this.lerpValue);
		peers[id].body.position.lerpVectors(peers[id].previousPosition,peers[id].desiredPosition, this.lerpValue);
        peers[id].head.quaternion.slerpQuaternions(peers[id].previousRotation,peers[id].desiredRotation, this.lerpValue);
      }
    }
  }

  updateClientVolumes() {
    for (let id in peers) {
      let audioEl = document.getElementById(id + "_audio");
      if (audioEl && peers[id].head) {
        let distSquared = this.camera.position.distanceToSquared(
          peers[id].head.position
        );

        if (distSquared > 500) {
          audioEl.volume = 0;
        } else {
          // from lucasio here: https://discourse.threejs.org/t/positionalaudio-setmediastreamsource-with-webrtc-question-not-hearing-any-sound/14301/29
          let volume = Math.min(1, 10 / distSquared);
          audioEl.volume = volume;
        }
      }
    }
  }

  //////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////
  // Interaction 🤾‍♀️

  getPlayerPosition() {
    // TODO: use quaternion or are euler angles fine here?
    return [
      [
        this.camera.position.x,
        this.camera.position.y,
        this.camera.position.z,
      ],
      [
        this.camera.quaternion._x,
        this.camera.quaternion._y,
        this.camera.quaternion._z,
        this.camera.quaternion._w,
      ],
    ];
  }

  //////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////
  // Rendering 🎥

  update() {
    requestAnimationFrame(() => this.update());
    this.frameCount++;
    if (this.frameCount % 25 === 0) {
      this.updateClientVolumes();
    }

    this.interpolatePositions();
    this.controls.update();
    this.render();
	
	//console.log("asd")
  }

  render() {
    this.renderer.render(this.scene, this.camera);
  }

  //////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////
  // Event Handlers 🍽

  onWindowResize(e) {
    this.width = window.innerWidth;
    this.height = Math.floor(window.innerHeight * 0.9);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.width, this.height);
  }
  
  
  //////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Utilities

	makeVideoMaterial(id) {
		let videoElement = document.getElementById(id + "_video");
		let videoTexture = new THREE.VideoTexture(videoElement);
		//videoTexture.flipY = false;
		let videoMaterial = new THREE.MeshBasicMaterial({
		map: videoTexture,
		side: THREE.DoubleSide,
		});
		
	return videoMaterial;
	}

}



	
	
////////////////////////////////////////////////////////////////////////////////
// Mint NFT:
////////////////////////////////////////////////////////////////////////////////
const checkIfWalletIsConnected = async () => {
	const { ethereum } = window;
	console.log("=== Checking if wallet is connected ===");

	if (!ethereum) {
		console.log("Make sure you have metamask!");
		// setCurrentAccount(null);
		return null;
	} else {
		web3 = new Web3(window.ethereum);
		console.log("We have the ethereum object", ethereum);
	}

	const accounts = await ethereum.request({ method: 'eth_accounts' });

	if (accounts.length !== 0) {
		const account = accounts[0];
		console.log("Found an authorized account:", account);
		return setCurrentAccount(account);
	} else {
		console.log("No authorized account found");
		return setCurrentAccount(null);
	}
};

const connectWallet = async () => {
	try {
		let web3 = await checkIfWalletIsConnected();
		const { ethereum } = window;

		if (!ethereum) {
			alert("Get MetaMask wallet ya peasant!");
			return;
		}

		/*
		* Fancy method to request access to account.
		*/
		const accounts = await ethereum.request({ method: "eth_requestAccounts" });

		/*
		* Boom! This should print out public address once we authorize Metamask.
		*/
		console.log("Connected", accounts[0]);
		setCurrentAccount(accounts[0]);
		enableMinting();
	} catch (error) {
		console.log(error);
	}
};

const askContractToMintNft = async () => {
	const CONTRACT_ADDRESS = "0xD1C108625Df6766bDf0Cd7c1e8c5dFAfC8D33080";

	try {
		const { ethereum } = window;

		if (ethereum) {
			const provider = new ethers.providers.Web3Provider(ethereum);
			const signer = provider.getSigner();
			const connectedContract = new ethers.Contract(CONTRACT_ADDRESS, renderPubNFT.abi, signer);

			console.log("Going to pop wallet now to pay gas...");
			let nftTxn = await connectedContract.makeAnEpicNFT();

			console.log("Mining...please wait.");
			await nftTxn.wait();

			console.log(`Mined, see transaction: https://rinkeby.etherscan.io/tx/${nftTxn.hash}`);
			console.log(`Mined, view NFT: https://testnets.opensea.io/assets/${nftTxn.hash}`);
		} else {
			console.log("Ethereum object doesn't exist!");
		}
	} catch (error) {
		console.log(error);
	}
};

function setCurrentAccount(account) {
	currentAccount = account;
	return currentAccount;
}

function mintThisSpace(){
	console.log("Good stuff here")
	// const TOTAL_MINT_COUNT = 50;
	connectWallet();
	currentAccount === "" ? (console.log("No mints here")) : (askContractToMintNft())
	// renderNotConnectedContainer()
}

function enableMinting() {
	console.log("=== Checknig if minting should be enabled ===")
	console.log(currentAccount);
	currentAccount === null ? (document.getElementById("hud").innerHTML = "Connect Wallet") : (document.getElementById("hud").innerHTML = "Mint This Space")
}


////////////////////////////////////////////////////////////////////////////////
// Start-Up Sequence:
////////////////////////////////////////////////////////////////////////////////

window.onload = async () => {
  console.log("Window loaded.");

	THREE = await import('./libs/three.module.js')
	const FirstPersonControlsModule = await import('./libs/FirstPersonControls.js')
	FirstPersonControls = FirstPersonControlsModule.FirstPersonControls
	
	
	//const GLTFLoaderModule = await import('https://cdn.skypack.dev/three@0.136.0/examples/jsm/loaders/GLTFLoader.js')
	const GLTFLoaderModule = await import('./libs/GLTFLoader.js')
	GLTFLoader = GLTFLoaderModule.GLTFLoader
	
	const RGBELoaderModule = await import('./libs/RGBELoader.js')
	RGBELoader = RGBELoaderModule.RGBELoader

	//const DRACOLoaderModule = await import('https://cdn.skypack.dev/three@0.136.0/examples/jsm/loaders/DRACOLoader.js')
	//DRACOLoader = DRACOLoaderModule.DRACOLoader;
	
	
	// Instantiate a loader
	loader = new GLTFLoader();
	
	const CSS2DRendererModule = await import('./libs/CSS2DRenderer.js')
	CSS2DRenderer = CSS2DRendererModule.CSS2DRenderer
	CSS2DObject  = CSS2DRendererModule.CSS2DObject
	
	
	// Optional: Provide a DRACOLoader instance to decode compressed mesh data
	//dracoLoader = new DRACOLoader();
	//dracoLoader.setDecoderPath('https://unpkg.com/three@0.136.0/examples/js/libs/draco/')
	//loader.setDRACOLoader( dracoLoader );
	//console.log(loader)
	
	
	//const FBXLoaderModule = await import('https://cdn.skypack.dev/three@0.136.0/examples/jsm/loaders/FBXLoader.js')
	//FBXLoader = FBXLoaderModule.FBXLoader;
	//FBXLoader1 = new FBXLoader();
	
				
				
	headMesh = await loader.loadAsync('../assets/meshes/HeadYup.glb');
	bodyMesh = await loader.loadAsync('../assets/meshes/body.glb');
	
	
	
	
	//console.log(result)

  
	// first get user media
	localMediaStream = await getMedia(mediaConstraints);

	createLocalVideoElement();
	
	var scene1Path = "../assets/brutalism_scene/scene.gltf"
	// finally create the threejs scene
	console.log("Creating three.js scene...");
	myScene = new Scene(scene1Path);
		
	var minter = document.getElementById("hud");
	minter.addEventListener('click', mintThisSpace)
	
	checkIfWalletIsConnected().then(
		function(value) { enableMinting() },
		function(error) {}
	);
	//console.log(headMesh)
  
    // then initialize socket connection
	initSocketConnection();

	// start sending position data to the server
	setInterval(function () {
		mySocket.emit("move", myScene.getPlayerPosition());
	}, 200);
};

////////////////////////////////////////////////////////////////////////////////
// Local media stream setup
////////////////////////////////////////////////////////////////////////////////

// https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
async function getMedia(_mediaConstraints) {
  let stream = null;

  try {
    stream = await navigator.mediaDevices.getUserMedia(_mediaConstraints);
  } catch (err) {
    console.log("Failed to get user media!");
    console.warn(err);
  }

  return stream;
}

////////////////////////////////////////////////////////////////////////////////
// Socket.io
////////////////////////////////////////////////////////////////////////////////

// establishes socket connection
function initSocketConnection() {
  console.log("Initializing socket.io...");
  mySocket = io();

  mySocket.on("connect", () => {
    console.log("My socket ID:", mySocket.id);
  });

  //On connection server sends the client his ID and a list of all keys
  mySocket.on("introduction", (otherClientIds) => {
    // for each existing user, add them as a client and add tracks to their peer connection
    for (let i = 0; i < otherClientIds.length; i++) {
      if (otherClientIds[i] != mySocket.id) {
        let theirId = otherClientIds[i];

        console.log("Adding client with id " + theirId);
        peers[theirId] = {};

        let pc = createPeerConnection(theirId, true);
        peers[theirId].peerConnection = pc;

        createClientMediaElements(theirId);

        myScene.addClient(theirId);

      }
    }
  });

  // when a new user has entered the server
  mySocket.on("newUserConnected", (theirId) => {
    if (theirId != mySocket.id && !(theirId in peers)) {
      console.log("A new user connected with the ID: " + theirId);

      console.log("Adding client with id " + theirId);
      peers[theirId] = {};

      createClientMediaElements(theirId);

      myScene.addClient(theirId);
    }
  });

  mySocket.on("userDisconnected", (clientCount, _id, _ids) => {
    // Update the data from the server
    if (_id != mySocket.id) {
      console.log("A user disconnected with the id: " + _id);
      myScene.removeClient(_id);
      removeClientVideoElementAndCanvas(_id);
      delete peers[_id];
    }
  });

  mySocket.on("signal", (to, from, data) => {
    // console.log("Got a signal from the server: ", to, from, data);

    // to should be us
    if (to != mySocket.id) {
      console.log("Socket IDs don't match");
    }

    // Look for the right simplepeer in our array
    let peer = peers[from];
    if (peer.peerConnection) {
      peer.peerConnection.signal(data);
    } else {
      console.log("Never found right simplepeer object");
      // Let's create it then, we won't be the "initiator"
      // let theirSocketId = from;
      let peerConnection = createPeerConnection(from, false);

      peers[from].peerConnection = peerConnection;

      // Tell the new simplepeer that signal
      peerConnection.signal(data);
    }
  });

  // Update when one of the users moves in space
  mySocket.on("positions", (_clientProps) => {
    myScene.updateClientPositions(_clientProps);
  });
}

////////////////////////////////////////////////////////////////////////////////
// Clients / WebRTC
////////////////////////////////////////////////////////////////////////////////

// this function sets up a peer connection and corresponding DOM elements for a specific client
function createPeerConnection(theirSocketId, isInitiator = false) {
  console.log('Connecting to peer with ID', theirSocketId);
  console.log('initiating?', isInitiator);

  let peerConnection = new SimplePeer({ initiator: isInitiator })
  // simplepeer generates signals which need to be sent across socket
  peerConnection.on("signal", (data) => {
    // console.log('signal');
    mySocket.emit("signal", theirSocketId, mySocket.id, data);
  });

  // When we have a connection, send our stream
  peerConnection.on("connect", () => {
    // Let's give them our stream
	
	// handle error: cannot read propert of undefined 'getTracks'
    peerConnection.addStream(localMediaStream);
    console.log("Send our stream");
  });

  // Stream coming in to us
  peerConnection.on("stream", (stream) => {
    console.log("Incoming Stream");

    updateClientMediaElements(theirSocketId, stream);
  });

  peerConnection.on("close", () => {
    console.log("Got close event");
    // Should probably remove from the array of simplepeers
  });

  peerConnection.on("error", (err) => {
    console.log(err);
  });

  return peerConnection;
}

// temporarily pause the outgoing stream
function disableOutgoingStream() {
	if(localMediaStream){
		localMediaStream.getTracks().forEach((track) => {
		track.enabled = false;
	  });
	}
  
}
// enable the outgoing stream
function enableOutgoingStream() {
	if(localMediaStream){
		localMediaStream.getTracks().forEach((track) => {
		track.enabled = true;
	  });
	}
  
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Utilities 🚂

// created <video> element for local mediastream
function createLocalVideoElement() {
  const videoElement = document.createElement("video");
  videoElement.id = "local_video";
  videoElement.autoplay = true;
  videoElement.width = videoWidth;
  videoElement.height = videoHeight;
  //videoElement.style = "visibility: hidden;";

  if (localMediaStream) {
    let videoStream = new MediaStream([localMediaStream.getVideoTracks()[0]]);

    videoElement.srcObject = videoStream;
  }
  document.body.appendChild(videoElement);
}

// created <video> element using client ID
function createClientMediaElements(_id) {
  console.log("Creating <html> media elements for client with ID: " + _id);

  const videoElement = document.createElement("video");
  videoElement.id = _id + "_video";
  videoElement.autoplay = true;
  // videoElement.style = "visibility: hidden;";

  document.body.appendChild(videoElement);

  // create audio element for client
  let audioEl = document.createElement("audio");
  audioEl.setAttribute("id", _id + "_audio");
  audioEl.controls = "controls";
  audioEl.volume = 1;
  document.body.appendChild(audioEl);

  audioEl.addEventListener("loadeddata", () => {
    audioEl.play();
  });
}

function updateClientMediaElements(_id, stream) {

  let videoStream = new MediaStream([stream.getVideoTracks()[0]]);
  let audioStream = new MediaStream([stream.getAudioTracks()[0]]);

  const videoElement = document.getElementById(_id + "_video");
  videoElement.srcObject = videoStream;

  let audioEl = document.getElementById(_id + "_audio");
  audioEl.srcObject = audioStream;
}

// remove <video> element and corresponding <canvas> using client ID
function removeClientVideoElementAndCanvas(_id) {
  console.log("Removing <video> element for client with id: " + _id);

  let videoEl = document.getElementById(_id + "_video");
  if (videoEl != null) {
    videoEl.remove();
  }
}
